LDFLAGS=$(shell pkg-config --libs relp) -g -Wall -Wextra -Werror
CFLAGS=$(shell pkg-config --cflags relp) -g -Wall -Wextra -Werror -std=gnu99

relper: relper.o

relper.o: relper.c

clean:
	rm -f *.o relper
