/* relper.c -- send messages via the RELP protocol

   Copyright 2013 Alexander Boström.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <librelp.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdarg.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

int debug = 0;

void debugprint(char *args, ...)
{
  va_list ap;
  va_start(ap, args);
  if (debug) vfprintf(stderr, args, ap);
  va_end(ap);
}

void xerr(char *s) {
  fprintf(stderr, s);
  exit(255);
}

void xmemerr() {
  xerr("Could not allocate memory.\n");
}

void *xmalloc(size_t size) {
  void *p = malloc(size);
  if (!p) xmemerr();
  return p;
}

void *xrealloc(void *ptr, size_t size) {
  void *p = realloc(ptr, size);
  if (!p) xmemerr();
  return p;
}

void strpalloc(char **strp, size_t size) {
  *strp = (char *)xmalloc(sizeof(char) * size);
}

void strprealloc(char **strp, size_t size) {
  *strp = (char *)xrealloc(*strp, sizeof(char) * size);
}

struct strbuf {
  char *s;
  size_t size;
};

struct strbuf *new_strbuf(size_t start_size) {
  struct strbuf *buf = (struct strbuf *)xmalloc(sizeof(struct strbuf));
  buf->s = (char *)xmalloc(sizeof(char) * start_size);
  buf->size = start_size;
  return buf;
}

void grow_strbuf(struct strbuf *buf) {
  buf->size *= 2;
  strprealloc(&(buf->s), buf->size);
}

void xusage(int usage, char *error) {
  FILE *out = stdout;
  if (error) {
    out = stderr;
    fprintf(out, "\nError: %s\n", error);
  }
  if (usage)
    fprintf(out, "\n"
"Usage:\n"
" relper [options] [message ...]\n"
"\n"
"Options:\n"
" -i, --id		log the process ID too\n"
" -f, --file <file>	log the contents of this file\n"
" -n, --server <name>	defaults to localhost.localdomain\n"
" -P, --port <number>	required\n"
" -p, --priority <prio>	mark given message with this priority\n"
" -s, --stderr		output message to standard error as well\n"
" -T, --tee		output message to standard out as well\n"
" -N, --program <name>	program name\n"
" -t, --tag <tag>	mark every line with this tag\n"
" -4			use only IPv4\n"
" -6			use only IPv6\n"
" -d, --debug		print debug messages\n"
" -h, --help\n"
" -V, --version\n"
);
  fprintf(out, "\n"
"relper, version 0.0.0\n"
"Copyright (C) 2013 Alexander Boström.\n"
"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"
"\n"
"This is free software; you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law.\n"
);
  if (error)
    exit(1);
  exit(0);
}

char *read_message(FILE *f, struct strbuf *buffer) {
  /* need to replace fgets() with something that can timeout
     because we want to read the next line too, but only if it is
     available */

  if (!fgets(buffer->s, buffer->size, f))
    return NULL;

  /* check what we got */
  size_t length = strlen(buffer->s);
  while (length > 0 && (buffer->s)[length-1] != '\r' && (buffer->s)[length-1] != '\n') {
    /* no newline at the end, did not get all of it */

    /* but don't accept more than we can send */
    if (buffer->size >= 65536)
      break; /* bug: we break even in the middle of an utf8 character */

    /* grow buffer and append */
    grow_strbuf(buffer);
    if (!fgets(&(buffer->s)[length], buffer->size-length, f))
      break;

    length = strlen(buffer->s);
  }

  /* strip newline */
  while (length > 0 && ((buffer->s)[length-1] == '\r' || (buffer->s)[length-1] == '\n' ))
    (buffer->s)[--length] = '\0';

  /* return value is valid until next call to this function */
  return buffer->s;
}

int
main (int argc, char *argv[])
{
  /* Boring option parsing */
  int opt_id = 0;
  int opt_stdin = 0;
  char *opt_file = NULL;
  char *opt_server = "localhost.localdomain";
  char *opt_port = NULL;
  char *opt_priority = NULL;
  int opt_tee = 0;
  int opt_stderr = 0;
  char *opt_program = NULL;
  char *opt_tag = NULL;
  char **msg = (char **)NULL;
  int opt_ipv4only = 0;
  int opt_ipv6only = 0;
  
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"id",		no_argument,		0, 0 },
      {"stdin",		no_argument,		0, 0 },
      {"file",		required_argument,	0, 0 },
      {"server",	required_argument,	0, 0 },
      {"port",		required_argument,	0, 0 },
      {"priority",	required_argument,	0, 0 },
      {"stderr",	no_argument,		0, 0 },
      {"tee",		no_argument,		0, 0 },
      {"program",	required_argument,	0, 0 },
      {"tag",		required_argument,	0, 0 },
      {"debug",		no_argument,		0, 0 },
      {"help",		no_argument,		0, 0 },
      {"version",	no_argument,		0, 0 },
      {0,		0,			0, 0 }
    };
    
    int c = getopt_long(argc, argv, "iIf:n:P:p:sTt:N46dhV",
		    long_options, &option_index);
    if (c == -1)
      break;

    /* Sorry about this mess. */
    switch (c) {
    case 0:
      if (strcmp(long_options[option_index].name, "id") == 0) { opt_id = 1;
      } else if (strcmp(long_options[option_index].name, "stdin") == 0) { opt_stdin = 1;
      } else if (strcmp(long_options[option_index].name, "file") == 0) {
	if (optarg) { opt_file = strdup(optarg); } else { xusage(1, "Missing argument to --file"); }
      } else if (strcmp(long_options[option_index].name, "server") == 0) {
	if (optarg) { opt_server = strdup(optarg); } else { xusage(1, "Missing argument to --server"); }
      } else if (strcmp(long_options[option_index].name, "port") == 0) {
	if (optarg) { opt_port = strdup(optarg); } else { xusage(1, "Missing argument to --port"); }
      } else if (strcmp(long_options[option_index].name, "priority") == 0) {
	if (optarg) { opt_priority = strdup(optarg); } else { xusage(1, "Missing argument to --priority"); }
      } else if (strcmp(long_options[option_index].name, "stderr") == 0) { opt_stderr = 1;
      } else if (strcmp(long_options[option_index].name, "tee") == 0) { opt_tee = 1;
      } else if (strcmp(long_options[option_index].name, "tag") == 0) {
	if (optarg) { opt_tag = strdup(optarg); } else { xusage(1, "Missing argument to --tag"); }
      } else if (strcmp(long_options[option_index].name, "program") == 0) {
	if (optarg) { opt_program = strdup(optarg); } else { xusage(1, "Missing argument to --program"); }
      } else if (strcmp(long_options[option_index].name, "help") == 0) {
	xusage(1, NULL);
      } else if (strcmp(long_options[option_index].name, "version") == 0) {
	xusage(0, NULL);
      }
      break;
    case 'i': opt_id = 1; break;
    case 'I': opt_stdin = 1; break;
    case 'f': if (optarg) { opt_file = strdup(optarg); } else { xusage(1, "Missing argument to -f"); } break;
    case 'n': if (optarg) { opt_server = strdup(optarg); } else { xusage(1, "Missing argument to -n"); } break;
    case 'P': if (optarg) { opt_port = strdup(optarg); } else { xusage(1, "Missing argument to -P"); } break;
    case 'p': if (optarg) { opt_priority = strdup(optarg); } else { xusage(1, "Missing argument to -p"); } break;
    case 's': opt_stderr = 1; break;
    case 'T': opt_tee = 1; break;
    case 't': if (optarg) { opt_tag = strdup(optarg); } else { xusage(1, "Missing argument to -t"); } break;
    case 'N': if (optarg) { opt_program = strdup(optarg); } else { xusage(1, "Missing argument to -N"); } break;
    case '4': opt_ipv4only = 1; break;
    case '6': opt_ipv6only = 1; break;
    case 'd': debug = 1; break;
    case 'h': xusage(1, NULL); break;
    case 'V': xusage(0, NULL); break;
    case '?': xusage(1, "Missing argument to an option."); break;
    default:
      fprintf(stderr, "Internal error: ?? getopt returned character code 0%d ??\n", c);
    }
  }

  if (optind < argc)
    msg = &argv[optind];

  if (opt_priority)
    xerr("Option --priority not implemented yet, sorry.");

  /* Further option checks */
  if (opt_stdin && opt_file)
    xusage(1, "Cannot combine --stdin and --file");
  if ((opt_stdin || opt_file) && msg)
    xusage(1, "Cannot combine --stdin/--file and a message on the combine line.");
  if (opt_port == NULL)
    xusage(1, "--port was not specified");
  if (opt_ipv4only && opt_ipv6only)
    xusage(1, "Cannot disable both IPv4 and IPv6.");

  if (opt_file == NULL && msg == NULL)
    opt_stdin = 1;

  /* Handle options */

  char *pidstring = "";
  if (opt_id)
    if (asprintf(&pidstring, "[%d]", getpid()) < 0)
      xmemerr();

  char *tagstring = "";
  if (opt_tag)
    if (asprintf(&tagstring, "%s: ", opt_tag) < 0)
      xmemerr();

  char *programstring = opt_program;
  if (!programstring) {
    programstring = "unknown";
    struct passwd *pwent = getpwuid(getuid());
    if (pwent)
      programstring = pwent->pw_name;
  }

  /* Not configurable */
  char nodename[256];
  strcpy(nodename, "unknown");
  gethostname(nodename, sizeof(nodename));
  char *firstdot = strchr(nodename, '.');
  if (firstdot)
    *firstdot = '\0';

  /* Connect and shuffle data */
  char *error_string = NULL;
  relpRetVal iRet = RELP_RET_OK;
  relpClt_t *relpconn;
  relpEngine_t *relpengine;

  error_string = "Could not initialize the RELP library.\n";
  CHKRet(relpEngineConstruct(&relpengine));
  CHKRet(relpEngineSetDbgprint(relpengine, debugprint));
  CHKRet(relpEngineCltConstruct(relpengine, &relpconn));
  CHKRet(relpEngineSetEnableCmd(relpengine, (unsigned char *)"syslog", eRelpCmdState_Required));

  int prot_a = PF_INET6, prot_b = PF_INET;

  if (opt_ipv4only)
    prot_a = PF_INET;
  if (opt_ipv6only || opt_ipv4only)
    prot_b = 0;

  error_string = "Could not connect to the RELP server.\n";
  relpRetVal ret = relpCltConnect(relpconn, prot_a, (unsigned char *)opt_port, (unsigned char *)opt_server);
  if (ret && prot_b && relpCltConnect(relpconn, prot_b, (unsigned char *)opt_port, (unsigned char *)opt_server) == 0)
    ret = 0;
  CHKRet(ret);

  error_string = "Could not send syslog data to the RELP server.\n";

  FILE *infile = NULL;
  if (opt_stdin)
    infile = fdopen(0, "r");
  if (opt_file)
    infile = fopen(opt_file, "r");

  struct strbuf *outbuf = new_strbuf(128);
  struct strbuf *inbuf = new_strbuf(128);

  while(1) {
    char *message = NULL;

    if (msg)
      message = *msg++;
    else
      message = read_message(infile, inbuf);

    if (!message)
      break;

    int ret;
    while ((int)outbuf->size <= (ret = snprintf(outbuf->s, outbuf->size, "%s %s%s %s%s", nodename, programstring, pidstring, tagstring, message)))
      grow_strbuf(outbuf);
    if (ret < 0)
      xerr("Internal error: snprintf()");

    CHKRet(relpCltSendSyslog(relpconn, (unsigned char *)(outbuf->s), strlen(outbuf->s)));

    if (opt_stderr)
      if (fprintf(stderr, "%s\n", outbuf->s) < 0)
	xerr("Output failure: stderr");
    if (opt_tee)
      if (printf("%s\n", outbuf->s) < 0)
	xerr("Output failure: stdout");
  }

  error_string = "Error while disconnecting from the RELP server.\n";
  CHKRet(relpEngineCltDestruct(relpengine, &relpconn));

  error_string = NULL;
  exit (EXIT_SUCCESS);

 finalize_it:
  if (error_string)
    fprintf(stderr, error_string);
  exit (EXIT_FAILURE);
}
